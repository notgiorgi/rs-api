<?php

require 'vendor/autoload.php';

use RS\Migrations\CreateWaybillsTable;
use RS\Migrations\CreateInvoicesTable;

try{
	CreateWaybillsTable::Migrate();
	echo CreateWaybillsTable::name . " Migration Success \n\r";
	CreateInvoicesTable::Migrate();
	echo CreateInvoicesTable::name . " Migration Success \n\r";
}
catch (\Exception $ex) {
	echo "Migration Failure: \n\r";
	echo "\t" . $ex->getMessage();
}
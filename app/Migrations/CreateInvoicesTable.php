<?php 

namespace RS\Migrations;

use RS\Models\Database;

/**
* Class for creating invoices table
*/
class CreateInvoicesTable
{
	const name = 'Create Invoices table';

	public function Migrate()
	{
		$invoices_table = file_get_contents(__dir__ . '/Sql/invoices.sql');
		$db = new Database();
		$res = $db->query($invoices_table);

		if(!$res){
			throw new Exception("Error migrating table");
		}

		$db->close();
		return true;
	}
}
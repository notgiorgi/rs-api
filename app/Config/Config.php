<?php 

namespace RS\Config;

/**
* Config Constants class
*/
class Config
{
	// * Configuration STEP 1: configure only DB config
	
	/*** DB Congif ***/
	const DB_HOST = 'localhost';
	const DB_USER = 'root';
	const DB_PASS = 'secret';
	const DB_NAME = 'rs-api';
	const DB_TYPE = 'mysql';
	const IP_ADDRESS = 'DB-ip';

	
	// * Configuration STEP 2: After testing API on test user, configure RS Config, you may also need to truncate database and even run migrations again
	
	/*** RS config ***/
	const USER_NAME = 'satesto2';
	const USER_PASSWORD = '123456';
	const TIN = '12345678910';

	const SERVICE_USER = 'NOTGIORGI:12345678910';
	const SERVICE_PASSWORD = '123123';


	const WAYBILL_URI = 'https://services.rs.ge/WayBillService/WayBillService.asmx?wsdl';
	const INVOICE_URI = 'https://www.revenue.mof.ge/ntosservice/ntosservice.asmx?WSDL';

	protected function __construct(){}
	private function __clone(){}
}
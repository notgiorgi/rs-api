<?php 

namespace RS\Services;

use RS\Config\Config;
use RS\Services\RS;


/**
* Waybill Class
*/
class RSWaybill extends RS
{
	function __construct()
	{
		parent::__construct();
		$this->uri = Config::WAYBILL_URI;
		$this->client = new \SoapClient($this->uri);
	}

	/**
	 * ფუნქცია აბრუნებს საქონლის ერთეულებს
	 * @return PHP Objects of parsed XML
	 */
	public function getWaybillUnits()
	{
		$response = $this->client->get_waybill_units([
			'su' => $this->su,
			'sp' => $this->sp
		]);

		return $this->parseResult($response->get_waybill_unitsResult->any);
	}

	/**
	 * ფუნქცია აბრუნებს ზედნადებების ტიპებს
	 * @return PHP Objects of parsed XML
	 */
	public function getWaybillTypes()
	{
		$response = $this->client->get_waybill_types([
			'su' => $this->su,
			'sp' => $this->sp
		]);

		return $this->parseResult($response->get_waybill_typesResult->any);
	}
	
	/**
	 * ფუნქცია აბრუნებს მყიდველის ზედნადებებს
	 * @return PHP Objects of parsed XML
	 */
	public function getBuyerWaybills()
	{
		$response = $this->client->get_buyer_waybills([
			'su' => $this->su,
			'sp' => $this->sp,
		]);
		return $this->parseResult($response->get_buyer_waybillsResult->any);
	}
}
<?php

namespace RS\Models;

use RS\Config\Config;
use RS\Models\Database;

/**
* Waybill MODEL Class to comunicate with DB
*/
class Waybill
{
	public $fields = [
		'ID','TYPE','CREATE_DATE','BUYER_TIN','BUYER_NAME','SELLER_NAME','SELLER_TIN','START_ADDRESS','END_ADDRESS','DRIVER_TIN','DRIVER_NAME','TRANSPORT_COAST','DELIVERY_DATE','STATUS','ACTIVATE_DATE','PAR_ID','FULL_AMOUNT','CAR_NUMBER','WAYBILL_NUMBER','CLOSE_DATE','S_USER_ID','BEGIN_DATE','IS_CONFIRMED','IS_CORRECTED','SELLER_ST'
	];

	public $table = 'waybills';

	
	function __construct($wbObj)
	{
		foreach ($this->fields as $key => $field) {
			if(isset($wbObj->$field)){
				$this->$field = $wbObj->$field;
			}
		}
	}

	public function display()
	{
		foreach ($this->fields as $key => $field) {
			if(isset($this->$field)){
				echo $field . ": " .$this->$field . "\r\n";
			}
		}
	}
}
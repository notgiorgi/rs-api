<?php

namespace RS\Models;
use RS\Config\Config;

/**
* Database PDO class
*/
class Database
{
	protected $hostname;

	protected $username;

	protected $password;

	protected $db_name;

	protected $db_type;

	protected $link;
	
	function __construct()
	{
		$this->hostname = Config::DB_HOST;
		$this->username = Config::DB_USER;
		$this->password = Config::DB_PASS;
		$this->db_name  = Config::DB_NAME;
		$this->db_type  = Config::DB_TYPE;

		$this->link = new \PDO("mysql:host=$this->hostname;dbname=$this->db_name;charset=utf8", $this->username, $this->password);		
	}

	public function save($obj)
	{
		$fields = "";
		$values = "";
		$update = "";
		
		foreach ($obj->fields as $key => $field) {
			if(isset($obj->$field)){
				$fields = $fields . $field . ",";
				$values = $values . "'" . $obj->$field . "',"; 
				$update = $update . $field . "='" . $obj->$field . "',";
			}
		}

		$fields = trim($fields, ",");
		$values = trim($values, ",");
		$update = trim($update, ",");
		
		$sql = "INSERT INTO $obj->table (${fields}) VALUES (${values}) ON DUPLICATE KEY UPDATE ${update}";

		return $this->link->query($sql);
	}

	public function query($sql)
	{
		return $this->link->query($sql);
	}

	public function close()
	{
		$this->link = null;
	}
}
<?php 

/**
 * @package RS Invoices
 * 
 * Get Invoices from RS and insert/update finnished ones in Database
 * @author Giorgi Bagdavadze <notgiorgi@gmail.com> 
 * Effort for us, Starry
 */


require 'vendor/autoload.php';

$logger = new Katzgrau\KLogger\Logger(__DIR__.'/logs');
$logger->info("Invoices Job run start");

try {	
	$invoiceService = new RS\Services\RSInvoice();

	$DB = new RS\Models\Database();
	if(!$DB){
		throw new Exception("Error connecting to DB");
	}


	$invs = $invoiceService->getBuyerInvoices();
	if(!isset($invs->invoices)){
		throw new Exception("No invoices fetched");
	}
		

	$invoices = array();
	foreach ($invs->invoices as $key => $value) {
		$tmp = new RS\Models\Invoice($value);
		$res = $DB->save($tmp);
		if(!$res){
			throw new Exception("Error saving data to db");
		}
		array_push($invoices, $tmp);
	}
	
	$logger->info(count($invoices) . " invoices fetched");
}
catch(Exception $ex){
	$logger->error($ex->getMessage());
	echo $ex->getMessage();
}
finally{
	$DB->close();
	$logger->info('Invoices Job run end');
}
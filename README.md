## rs.ge Waybill and invoice API PHP implementation

Giorgi Bagdavadze <notgiorgi@gmail.com>

####Requirements:
- composer
- PHP 5.6

####Installation:

1. Clone repository
2. run ````composer install````
3. configure ````app/Config/Config.php```` (Follow the comments)
4. run ````php migrate.php````

####Usage:
After Installation procedure, you can use ````getInvoices.php```` or ````getWaybills.php```` to Get waybills and invoices.